<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'docker_description' => 'Interface pour l\'importation par lots de documents distants.',
	'docker_nom' => 'Docker',
	'docker_slogan' => 'Importe les documents distants par lots',
);

?>