<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

$version_spip = substr(spip_version(), 0, 1);

/* Assurer la compatibilite transitoire */
if($version_spip == 3){
	include_spip('inc/distant_spip3.php');
}
if($version_spip == 4){
	include_spip('ecrire/inc/distant');
}
